import 'package:get/get.dart';

class ThemeController extends GetxController {
  late bool isDark;
  ThemeController() {
    isDark = false;
  }

  isDarkOperation(bool value) {
    isDark = value;
    update();
  }
}
