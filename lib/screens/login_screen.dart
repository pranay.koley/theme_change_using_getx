// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:theme_change_using_getx/screens/home_screen.dart';
import 'package:theme_change_using_getx/themeController.dart';
import 'package:theme_change_using_getx/themes/theme_data.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: 20,
                      width: 20,
                      child: Image.asset('assets/bulb.png'),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    GetBuilder<ThemeController>(builder: (con) {
                      return Switch(
                        value: con.isDark,
                        onChanged: (value) async {
                          con.isDark
                              ? con.isDarkOperation(value)
                              : con.isDarkOperation(value);
                          debugPrint(con.isDark.toString());
                        },
                        activeTrackColor: Colors.yellow,
                        inactiveThumbColor: Colors.redAccent,
                        inactiveTrackColor: Colors.orange,
                      );
                      // return IconButton(
                      //     onPressed: () {
                      //       debugPrint(con.isDark.toString());
                      //       // ThemeController().isDark
                      //       //     ? ThemeController().isDark = true
                      //       //     : ThemeController().isDark = false;
                      //       con.isDark
                      //           ? con.isDarkOperation(false)
                      //           : con.isDarkOperation(true);
                      //       debugPrint(con.isDark.toString());
                      //     },
                      //     icon: Icon(Icons.dark_mode));
                    }),
                    SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      height: 20,
                      width: 20,
                      child: Image.asset('assets/moon.png'),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 120,
              ),
              GetBuilder<ThemeController>(
                builder: (
                  con,
                ) {
                  return Text(
                    'Welcome',
                    style: TextStyle(
                      fontSize: 50,
                      color: con.isDark ? Colors.white : Colors.purpleAccent,
                    ),
                  );
                },
              ),
              SizedBox(
                height: 150,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Username',
                      contentPadding: const EdgeInsets.all(15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30))),
                  onChanged: (value) {
                    // do something
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Password',
                      contentPadding: const EdgeInsets.all(15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30))),
                  onChanged: (value) {
                    // do something
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                    height: 50,
                    width: double.infinity,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    child: ElevatedButton(
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ))),
                        onPressed: (() {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => HomeScreen(),
                              ));
                        }),
                        child: Text(
                          'Submit',
                          style: TextStyle(fontSize: 25),
                        ))),
              ),
              SizedBox(
                height: 180,
              ),
            ],
          ),
        ),
      )),
    );
  }
}
