// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:theme_change_using_getx/screens/login_screen.dart';
import 'package:theme_change_using_getx/themeController.dart';
import 'package:theme_change_using_getx/themes/theme_data.dart';

void main() {
  Get.put(ThemeController());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(builder: (controller) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: controller.isDark ? Themes.dark : Themes.light,
        home: LoginScreen(),
      );
    });
  }
}
